package supero.com.helloworld;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Carlos Lima on 1/23/2017.
 */
public class HelloWorld extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity);

    }

}
